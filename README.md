# README #

### Drupal News Page Feature ###

* Version - 7.x-1.0

### How do I get set up? ###

* Make sure you have the Features module installed and enabled (https://www.drupal.org/project/features)
* Download this iBec News Feature repository
* Put the ibec_news folder with all of the module files in the sites/all/modules directory of your site
* Go to the Modules page in the admin of your site and enable this new feature, it will be called News
* Once the feature/module is enabled, you will see a new content type named News
* Also included is a _News_ View
* There is a template included with this feature. It will be in the sites/all/modules/ibec_news/templates folder.
* If you need to make changes to the template, copy the template to your theme templates folder

### Contribution guidelines ###

* Make any changes to this module at features.ibec.me
* Recreate the feature
* Commit new version of the feature as a whole to Bitbucket

### Admin ###

* Matt Smith